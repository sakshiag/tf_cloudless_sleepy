# Configure these variables

variable "sub_var" {
  description = "A sub_var to pass to the template."
  default     = "sub_hello"
}
